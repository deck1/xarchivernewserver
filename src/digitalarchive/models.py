from django.db import models
import time
import random
from djongo import models as modeldb



class ImageResult(models.Model):
    class Meta:
        db_table="digitalarchive_imageresult"

    name=models.CharField(max_length=300)
    x=models.FloatField()
    y=models.FloatField()
    height=models.FloatField()
    width=models.FloatField()
    imagedata=models.CharField(max_length=1000)
    def __str__(self):
        return self.name

class ImageReference(models.Model):
    class Meta:
        db_table="digitalarchive_imagereference"

    id=modeldb.IntegerField(primary_key=True)
    bdno=models.CharField(max_length=300)
    imagename=models.CharField(max_length=300)
    imagedirectory=models.CharField(max_length=300)

class User(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="tbl_user"
    
    name=models.CharField(max_length=300)
    loginid=models.CharField(max_length=300)
    password=models.CharField(max_length=300)

    def __str__(self):
        return self.name

class UserRole(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="user_role"
        pass

    loginid=models.CharField(max_length=300)
    role=models.CharField(max_length=300)

    def __str__(self):
        return self.loginid



 
class BusinessObject(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="business_object"

    name=models.CharField(max_length=300)

    def __str__(self):
        return self.name

class Status(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="status"
        
    name=models.CharField(max_length=300)
    def __str__(self):
        return self.name

class Action(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="action"
        
    name=models.CharField(max_length=300)

    def __str__(self):
        return self.name

class Edge(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="edge"

    objectname=models.CharField(max_length=300)
    nextaction=models.CharField(max_length=300)
    startstatus=models.CharField(max_length=300)
    endstatus=models.CharField(max_length=300)
    role=models.CharField(max_length=300)

    def __str__(self):
        return self.objectname


class CrudEdge(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="edge_crud"

    objname=models.CharField(max_length=300)
    status=models.CharField(max_length=300)
    role=models.CharField(max_length=300)
    crudaction=models.CharField(max_length=300)

    def __str__(self):
        return self.objname


class BoFieldEditNoAccess(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="bo_fields_edit_no_access"
    
    user=models.CharField(max_length=300)
    role=models.CharField(max_length=300)
    businessobject=models.CharField(max_length=300)
    attribute=models.CharField(max_length=300)

    def __str__(self):
        return self.user

class BoFieldViewNoAccess(models.Model):
    class Meta:
        app_label = 'postgres'
        db_table="bo_fields_view_no_access"
    
    user=models.CharField(max_length=300)
    role=models.CharField(max_length=300)
    businessobject=models.CharField(max_length=300)
    attribute=models.CharField(max_length=300)

    def __str__(self):
        return self.user

class Urls(models.Model):
    url = models.CharField(max_length=200)
    class Meta:
        abstract = True


class RankHistory(models.Model):
    sn = models.CharField(max_length=200)
    rank = models.CharField(max_length=200)
    startdate = models.CharField(max_length=200)
    enddate = models.CharField(max_length=200)
    class Meta:
        abstract = True

class Personnel(modeldb.Model):
    class Meta:
        db_table="digitalarchive_personnel"
    id=modeldb.IntegerField(primary_key=True)
    bdno = modeldb.CharField(max_length=500)
    name= modeldb.CharField(max_length=500)
    isretired= modeldb.CharField(max_length=500)
    status = modeldb.CharField(max_length=500)
    currentrank=modeldb.CharField(max_length=500)
    nameplate=modeldb.CharField(max_length=500)
    rankhistory=modeldb.ArrayField(
        model_container=RankHistory
    )
    imageurls= modeldb.ArrayField(
        model_container=Urls
    )
    


class Rank(models.Model):
    class Meta:
        db_table="digitalarchive_rank"
    name=models.CharField(max_length=500)
    code=models.CharField(max_length=500)
    description=models.CharField(max_length=500)
    rankno=models.IntegerField()
    orderid = models.IntegerField()

    def __str__(self):
        return self.name

class PersonnelAsset(models.Model):
    sn=models.CharField(max_length=500)
    bdno=models.CharField(max_length=500)
    rank=models.CharField(max_length=500)
    name=models.CharField(max_length=500)
    rankinpic=models.CharField(max_length=500)
    nameplate=models.CharField(max_length=500)
    x=models.FloatField()
    y=models.FloatField()
    width=models.FloatField()
    height=models.FloatField()
    marked=models.CharField(max_length=500)
    isManush=models.CharField(max_length=500)
    class Meta:
        abstract = True

class EventAsset(models.Model):
    name=models.CharField(max_length=500)
    class Meta:
        abstract = True

class LocationAsset(models.Model):
    name=models.CharField(max_length=500)
    class Meta:
        abstract = True

class AircraftAsset(models.Model):
    sn=models.CharField(max_length=500)
    name=models.CharField(max_length=500)
    class Meta:
        abstract = True


class Secrecy(models.Model):
    class Meta:
        db_table="digitalarchive_secrecy"
    name=models.CharField(max_length=500)
    def __str__(self):
        return self.name

class StudioPhotography(models.Model):
    class Meta:
        db_table="digitalarchive_studiophotography"
    name=models.CharField(max_length=500)
    def __str__(self):
        return self.name

class Comment(models.Model):
    sn = models.IntegerField()
    commentFrom = models.CharField(max_length=300)
    dt = models.CharField(max_length=300)
    comment = models.CharField(max_length=300)
    class Meta:
        abstract = True

class Asset(models.Model):
    class Meta:
        db_table="digitalarchive_asset"
    id=modeldb.IntegerField(primary_key=True)
    assetid=models.CharField(max_length=500)
    title=models.CharField(max_length=4096)
    description=models.CharField(max_length=500)
    taken=models.CharField(max_length=500)
    imageurls=models.CharField(max_length=500)
    status=models.CharField(max_length=500)
    imagename=models.CharField(max_length=500)
    galurl=models.CharField(max_length=500)
    secrecylevel=models.CharField(max_length=500)
    studiophotography=models.CharField(max_length=500)
    caption=models.CharField(max_length=500)
    directory=models.CharField(max_length=500)
    comment=modeldb.ArrayField(
        model_container=Comment
    )
    personnel=modeldb.ArrayField(
        model_container=PersonnelAsset
    )
    event=modeldb.ArrayField(
        model_container=EventAsset
    )
    location=modeldb.ArrayField(
        model_container=LocationAsset
    )
    aircraft=modeldb.ArrayField(
        model_container=AircraftAsset
    )
    othertags=modeldb.ArrayField(
        model_container=LocationAsset
    )
    originalimageurl=models.CharField(max_length=500)
    isrecognised=models.CharField(max_length=500)
    originalimagename=models.CharField(max_length=500)
    def __str__(self):
        return self.assetid


class Events(models.Model):
    class Meta:
        db_table="digitalarchive_events"
    name=models.CharField(max_length=500)
    date=models.CharField(max_length=500)
    description=models.CharField(max_length=500)
    def __str__(self):
        return self.name

class Location(models.Model):
    class Meta:
        db_table="digitalarchive_location"
    name=models.CharField(max_length=500)
    address=models.CharField(max_length=500)
    lat=models.FloatField(max_length=500)
    lng=models.FloatField(max_length=500)

    def __str__(self):
        return self.name


class Aircrafts(models.Model):
    class Meta:
        db_table="digitalarchive_aircrafts"
    id=modeldb.IntegerField(primary_key=True)
    aircraftid=models.CharField(max_length=500)
    name=models.CharField(max_length=500)
    description=models.CharField(max_length=500)
    status=models.CharField(max_length=500)
    imageurls= modeldb.ArrayField(
        model_container=Urls
    )

    def __str__(self):
        return self.aircraftid


# Create your models here.
