from django.shortcuts import render
from .models import *
from .serializers import *
from rest_framework import generics,mixins
import requests
import face_recognition
import numpy as np
from PIL import Image, ImageDraw
import base64
from io import BytesIO
import urllib.request
import uuid
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework.decorators import api_view
import pymongo
from rest_framework.response import Response
import json
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.pagination import PageNumberPagination
from elasticsearch import Elasticsearch 
from elasticsearch_dsl import Search
from elasticsearch_dsl import Q
from .externalurl import *
import os
import shutil


# es=Elasticsearch([{'host':elastichost,'port':elasticport}],http_auth=(elasticuser,elasticpass))
es=Elasticsearch(hosts=elastichost,verify_certs=False)
searchApi='archive'
image_url=extimage_url
myclient = pymongo.MongoClient(extmonngouri)
mydb = myclient["digitalarchievemongo"]

class Authentication(generics.ListAPIView):
    resource_name = 'logauths'
    serializer_class = UserSerializer

    def get_queryset(self):
        user = self.request.GET['query[value1]']
        password=self.request.GET['query[value]']
        return User.objects.filter(loginid=user,password=password)

class NextAllowableCrudAction(generics.ListAPIView):
    resource_name = 'nextcrudactions'
    serializer_class = CrudEdgeSerializer
    
    def get_queryset(self):
        data=self.request.GET  
        array = data['status'].split(',')
        return CrudEdge.objects.filter(role=data['role'],status__in=array,objname=data['objname'])



class NextAllowableActions(generics.ListAPIView):
    resource_name = 'nextallowableactions'
    serializer_class = EdgeSerializer

    def get_queryset(self):
        data=self.request.GET
        arrayObjName = data['objectname'].split(',')
        array = data['endstatus'].split(',')
        return Edge.objects.filter(objectname__in=arrayObjName,startstatus=data['startstatus'],endstatus__in=array,role=data['role'])

class PersonnelApi(mixins.CreateModelMixin, generics.ListAPIView): 
    resource_name = 'personnels'
    serializer_class= PersonnelSerializer
    
    def get_queryset(self):
        return Personnel.objects.all() 
    
    def post(self, request, *args, **kwargs):
        imgarray =  request.data['imagedataarray']
        personnel_url=image_url+'/api/v1/personnelimageupload'
        for d in imgarray:
            payload={}
            payload['imagename']=d['imagename']
            payload['image']=d['imagedata']
            payload['directory']=request.data['bdno']
            result=requests.post(personnel_url,data=payload)
            pass
        cnt=Personnel.objects.count()
        cnt=cnt+1
        request.data['id']=cnt
        return self.create(request, *args, **kwargs)

class PersonnelDetail(generics.RetrieveUpdateDestroyAPIView): 
    resource_name = 'personnels'
    lookup_field  = 'id'
    serializer_class = PersonnelSerializer

    def get_queryset(self):
        return Personnel.objects.all() 

class RankApi(mixins.CreateModelMixin, generics.ListAPIView):
    resource_name = 'ranks'
    serializer_class= RankSerializer
    
    def get_queryset(self):
        return Rank.objects.all().order_by('id')
    
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

class GetNotEditableFields(generics.ListAPIView):
    resource_name = 'bofieldseditnoaccesses'
    serializer_class = BoFieldEditNoAccessSerializer
    def get_queryset(self):
        data=self.request.GET
        return BoFieldEditNoAccess.objects.filter(user=data['user'],role=data['role'],businessobject=data['businessobject'])

class GetNotViewableFields(generics.ListAPIView):
    resource_name = 'bofieldsviewnoaccesses'
    serializer_class = BoFieldViewNoAccessSerializer
    def get_queryset(self):
        data=self.request.GET
        return BoFieldViewNoAccess.objects.filter(user=data['user'],role=data['role'],businessobject=data['businessobject'])


class EventApi(mixins.CreateModelMixin, generics.ListAPIView):
    resource_name = 'events'
    serializer_class= EventsSerializer
    
    def get_queryset(self):
        return Events.objects.all() 
    
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

class LocationApi(mixins.CreateModelMixin, generics.ListAPIView):
    resource_name = 'locations'
    serializer_class= LocationSerializer
    
    def get_queryset(self):
        return Location.objects.all() 
    
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

class SecrecyApi(mixins.CreateModelMixin, generics.ListAPIView):
    resource_name = 'secrecies'
    serializer_class= SecrecySerializer
    
    def get_queryset(self):
        return Secrecy.objects.all() 
    
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class StudioPhotographyApi(mixins.CreateModelMixin, generics.ListAPIView):
    resource_name = 'studiophotographies'
    serializer_class= StudioPhotographySerializer
    
    def get_queryset(self):
        return StudioPhotography.objects.all() 
    
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

class AircraftDetail(generics.RetrieveUpdateDestroyAPIView): 
    resource_name = 'aircrafts'
    lookup_field  = 'id'
    serializer_class = AircraftSerializer
    def get_queryset(self):
        return Aircrafts.objects.all()

class AircraftApi(mixins.CreateModelMixin, generics.ListAPIView):
    resource_name = 'aircrafts'
    serializer_class= AircraftSerializer
    
    def get_queryset(self):
        return Aircrafts.objects.all() 
    
    def post(self, request, *args, **kwargs):
        imgarray =  request.data['imagedataarray']
        aircraft_url=image_url+'/api/v1/aircraftimageupload'
        cnt=Asset.objects.count()
        directoset=request.data['aircraftid']
        cnt=cnt+1
        request.data['id']=cnt
        request.data['aircraftid']="AIRCRAFT-"+str(cnt)
        for d in imgarray:
            payload={}
            payload['imagename']=d['imagename']
            payload['image']=d['imagedata']
            payload['directory']=directoset
            result=requests.post(aircraft_url,data=payload)
            pass
        return self.create(request, *args, **kwargs)



class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 10


class GalleryApi(generics.ListAPIView):
    resource_name = 'galleries'
    serializer_class= GallerySerializer

    def get_queryset(self):
        try:
            print()
            return Asset.objects.all()
            pass
        except OSError as e:
            print(e)
            pass
       

class TotalSearchApi(generics.ListAPIView):
    resource_name = 'searches'
    serializer_class= SearchSerializer
    def get_queryset(self):
        idarray=[]
        s = Search(using=es)
        q = Q("multi_match", query=self.request.GET['filter'], fields=['othertags.name','title','location.name','personnel.rank','personnel.name','personnel.nameplate','event.name'])
        search = s.query(q)
        for data in search:
            idarray.append(data.assetid)
            pass
        return Asset.objects.filter(assetid__in=idarray)


class AdvanceSearchApi(generics.ListAPIView):
    resource_name = 'advancesearches'
    serializer_class= AdvanceSearchSerializer
    
    def get_queryset(self):
        s = Search(using=es)
        location=''
        events=''
        names=''
        bdno=''
        titles=''
        locations=''
        startdate=''
        enddate=''
        idarray=[]
     
        if('bdno' in self.request.GET):
            bdno=self.request.GET['bdno']
               
        if('titles' in self.request.GET):
            titles=self.request.GET['titles']
        
        if('names' in self.request.GET):
            names=self.request.GET['names']
        
        if('location' in self.request.GET):
            locations=self.request.GET['location']
        
        if('events' in self.request.GET):
            events=self.request.GET['events']
        
        if('startdate' in self.request.GET):
            startdate=self.request.GET['startdate']

        if('enddate' in self.request.GET):
            enddate=self.request.GET['enddate']    

        if(self.request.GET['operator']=="and"):
            t=Q('match',title=titles)
            l=Q('match',location__name=locations)
            n=Q('match',personnel__name=names)
            b=Q('match',personnel__bdno=bdno)
            e=Q('match',event__name=events)
            q = Q('bool',should=[t,l,n,b,e],minimum_should_match=1)
            search = s.query(q)
            for data in search:
                idarray.append(data.assetid)
                pass
            pass
        else:
            t=Q('match',title=titles)
            l=Q('match',location__name=locations)
            n=Q('match',personnel__name=names)
            b=Q('match',personnel__bdno=bdno)
            e=Q('match',event__name=events)
            q = Q('bool',should=[t,l,n,b,e],minimum_should_match=1)
            search = s.query(q)
            for data in search:
                idarray.append(data.assetid)
                pass

            pass
        return Asset.objects.filter(assetid__in=idarray)

class AssetApi(mixins.CreateModelMixin, generics.ListAPIView):
    resource_name = 'assets'
    serializer_class= AssetSerializer
    
    def get_queryset(self):
        data=self.request.GET
        if('directory' in data):
            print(data['directory'])
            if(data['directory']=='/BAFArchive/'):
                return Asset.objects.all() 
                pass
            else:
                return Asset.objects.filter(directory=data['directory'])
            pass
        else:
            return Asset.objects.all() 
            pass
        
    
    def post(self, request, *args, **kwargs):
        try:
            aircraft_url=image_url+'/api/v1/assetimageupload'
            cnt=Asset.objects.count()
            cnt=cnt+1
            imagename=request.data['imagename']
            imageoriname=request.data['originalimagename']
            request.data['assetid']="ASSET-"+str(cnt)
            request.data['id']=cnt
            payload={}
            payload['imagename']=imagename
            payload['image']=request.data['imagedataarray']
            payload['oriimagename']=imageoriname
            payload['oriimage']=request.data['oriimagedataarray']

            result=requests.post(aircraft_url,data=payload)
            searchdata=request.data
            searchdata.pop('imagedataarray', None)
            searchdata.pop('oriimagedataarray', None)
            searchdata.pop('personnel', None)
            if(es.indices.exists(index=searchApi)):
                pass
            else:
                request_body = {
                    "settings": {
                        "number_of_shards": 1,
                        "number_of_replicas": 0
                    }
                }
                res = es.indices.create(index=searchApi, body=request_body, ignore=400, request_timeout=10)
                pass
            if(request.data["isSearch"]=="1"):
                res=es.index(index=searchApi,id=searchdata['assetid'],body=searchdata)
                pass
            pass
        except Exception as e:
            print(e)
            pass
        return self.create(request, *args, **kwargs)
        

class AssetAllApi(mixins.CreateModelMixin, generics.ListAPIView):
    resource_name = 'assetalls'
    serializer_class= AssetSerializer

    def get_queryset(self):

        return Asset.objects.all()
        

class AssetDetail(generics.RetrieveUpdateDestroyAPIView): 
    resource_name = 'assets'
    lookup_field  = 'id'
    serializer_class = AssetSerializer

    def get_queryset(self):
        if(self.request.method == "PATCH"):
            personnel_url=image_url+'/api/v1/personnelimageupload'
            personnels=self.request.data['personnel']
            image=self.request.data['imagename']
            im = Image.open(requests.get(self.request.data['imageurls'], stream=True).raw)
            width, height = im.size
            wratio=width/700
            hratio=height/525
            searchdata=self.request.data
            searchdata.pop('imagedataarray', None)
            searchdata.pop('oriimagedataarray', None)
            personel=searchdata["personnel"]
            searchdata.pop('personnel', None)
            for d in personel:
                d.pop("marked")
                d.pop("isManush")
                pass
            searchdata["personnel"]=personel
            if(es.indices.exists(index=searchApi)):
                pass
            else:
                request_body = {
                    "settings": {
                        "number_of_shards": 1,
                        "number_of_replicas": 0
                    }
                }
                res = es.indices.create(index=searchApi, body=request_body, ignore=400, request_timeout=10)
                pass
            res=es.index(index=searchApi,id=searchdata['assetid'],body=searchdata) 
            for personnel in personnels:
                cy=float(personnel['y'])* hratio
                cx=float(personnel['x'])* wratio
                cw=float(personnel['width'])*wratio
                ch=float(personnel['height'])*hratio
                top=cy
                bottom=cy+ch
                right=cx+cw
                left=cx
                cropped =im.crop((left, top, right, bottom))
                buff = BytesIO()
                cropped.save(buff, format="JPEG")
                img_str = base64.b64encode(buff.getvalue())
                personnelCnt=Personnel.objects.filter(bdno = personnel["bdno"]).count()
                if(personnelCnt==0):
                    rank=[{"sn":0,"rank":"N/A","startdate":"N/A","enddate":"N/A"}]
                    personnelurl=image_url+"/static/personnel/"+personnel["bdno"]+"_1.jpg"
                    url=[{"url":personnelurl}]
                    name=personnel["bdno"]+"_1.jpg"
                    payload={}
                    payload['imagename']=name
                    payload['image']=img_str
                    result=requests.post(personnel_url,data=payload)
                    Personnel.objects.create(id=1,bdno=personnel["bdno"],name=personnel["name"],isretired="0",currentrank=personnel["rank"],nameplate="N/A",rankhistory=rank,imageurls=url,status="draft")
                    cnt=ImageReference.objects.count()
                    cnt=cnt+1
                    ImageReference.objects.create(id=cnt,bdno=personnel["bdno"],imagename=name,imagedirectory=personnelurl)
                    pass
                else:
                    persons=Personnel.objects.filter(bdno = personnel["bdno"]).all()
                    personarray=[]
                    for person in persons:
                        print(len(person.imageurls))
                        for perurl in person.imageurls:
                            personarray.append(perurl)
                            pass
                        pass
                    cnt=len(personarray)+1
                    personnelurl=image_url+"/static/personnel/"+personnel["bdno"]+"_"+str(cnt)+".jpg"
                    name=personnel["bdno"]+"_"+str(cnt)+".jpg"
                    payload={}
                    payload['imagename']=name
                    payload['image']=img_str
                    result=requests.post(personnel_url,data=payload)
                    personarray.append({"url":personnelurl})
                    Personnel.objects.filter(bdno = personnel["bdno"]).update(imageurls=personarray)
                    cnt=ImageReference.objects.count()
                    cnt=cnt+1
                    ImageReference.objects.create(id=cnt,bdno=personnel["bdno"],imagename=name,imagedirectory=personnelurl)
                    
                    pass

                pass
            pass
        assets=Asset.objects.all()
        return assets


class ImageReferenceApi(mixins.CreateModelMixin, generics.ListAPIView):
    resource_name = 'imagereferences'
    serializer_class= ImageReferenceSerializer
    
    def get_queryset(self):
        return ImageReference.objects.all() 
    
    def post(self, request, *args, **kwargs):
        cnt=ImageReference.objects.count()
        cnt=cnt+1
        request.data['id']=cnt
        return self.create(request, *args, **kwargs)


@api_view(['GET'])
def galleryDirectory(request):
    mycol = mydb["gallerydirectory"]
    data=mycol.find()
    array=[]
    for x in data:
        array=x['data']
    return JsonResponse({'result': array}, status=200)

@api_view(['GET','POST'])
def insertgalleryDirectory(request):
    mycol = mydb["gallerydirectory"]
    coldict={'data':json.loads(request.data['data'])}
    result = mycol.remove() 
    x = mycol.insert_one(coldict)
    return JsonResponse({'result': "success"}, status=200)

@api_view(['GET','POST'])
def searchgallerydir(request):
    directory=request.data['dir']
    array=[]
    data=""
    data=Asset.objects.filter(directory=directory).all()
    paginator = StandardResultsSetPagination()
    result_page = paginator.paginate_queryset(data, request)

    for d in result_page:
        dict={}
        dict['assetid']=d.assetid
        dict['directory']=d.directory
        dict['id']=d.id
        dict['imageurls']=d.imageurls
        dict['taken']=d.taken
        dict['title']=d.title
        array.append(dict)
        pass
    return JsonResponse({'result': array}, status=200)

@api_view(['POST','GET'])
def bulkasset(request):
    try:
        datas=json.loads(request.data.get('data'))
        for data in datas:
            aircraft_url=image_url+'/api/v1/assetimageupload'
            cnt=Asset.objects.count()
            cnt=cnt+1
            imagename=data['imagename']
            imageoriname=data['originalimagename']
            data['assetid']="ASSET-"+str(cnt)
            data['id']=cnt
            payload={}
            payload['imagename']=imagename
            payload['image']=data['imagedataarray']
            payload['oriimagename']=imageoriname
            payload['oriimage']=data['oriimagedataarray']
            result=requests.post(aircraft_url,data=payload)
            data.pop('imagedataarray', None)
            data.pop('oriimagedataarray', None)
            data.pop('isSearch', None)
            row = mydb["digitalarchive_asset"]
            res = row.insert_one(data)
            pass
        pass
    except Exception as e:
        print(e)
        pass
    return JsonResponse({'result': "success"}, status=200)
    
@api_view(['POST','GET'])       
def imagerecognition(request):
    try:
        array=[]
        face_names = []
        known_face_encodings = []
        known_face_names = []
        sn=1
        face_locations = []
        image = request.data['imagedata']
        starter = image.find(',')
        image_data = image[starter+1:]
        image_data = bytes(image_data, encoding="ascii")
        cnt=ImageReference.objects.count()
        if(cnt>0):
            datas = ImageReference.objects.all()
            im = Image.open(BytesIO(base64.b64decode(image_data)))
            width, height = im.size
            name=str(uuid.uuid1())
            dirname = os.path.dirname(__file__)
            filename = os.path.join(dirname, 'static')
            if im.mode in ["RGBA", "P"]:
                im = im.convert("RGB")
                im.save(filename + "/" + name +'.jpg',format='JPEG', quality=95)
                pass
            else:
                im.save(filename + "/" + name +'.jpg', 'JPEG')
                pass 
            
            unknown_image = face_recognition.load_image_file(filename + "/" + name +'.jpg')
            face_locations = face_recognition.face_locations(unknown_image)
            face_encodings = face_recognition.face_encodings(unknown_image, face_locations)
            for data in datas:
                response = urllib.request.urlopen(data.imagedirectory)
                image = face_recognition.load_image_file(response)
                if(len(face_recognition.face_encodings(image))>0):
                    known_face_encoding = face_recognition.face_encodings(image)[0]
                    known_face_encodings.append(known_face_encoding)
                    known_face_names.append(data.bdno)
                pass

            for (top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):
                matches = face_recognition.compare_faces(known_face_encodings, face_encoding)
                name = ""
                face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
                best_match_index = np.argmin(face_distances)
                if matches[best_match_index]:
                    name = known_face_names[best_match_index]
                    pass
                
                newdict={}
                newdict['sn']=sn
                newdict['bdno']=name
                newdict['left']=left
                newdict['top']=top
                newdict['right']=right
                newdict['bottom']=bottom
                newdict['iwidth']=width
                newdict['iheight']=height
                if(name != ""):
                    data=Personnel.objects.filter(bdno=name).all()
                    for d in data:
                        newdict['name']=d.name
                        newdict['rank']=d.currentrank
                        newdict['nameplate']=d.nameplate
                        pass
                    pass
                else:
                    newdict['name']=""
                    newdict['rank']=""
                    newdict['nameplate']=""
                    pass
                
                array.append(newdict)
                sn=sn+1

            pass
        pass
    except OSError as e:
        print(e)
        pass
    return JsonResponse({'result': array}, status=200)
        

    
    
  