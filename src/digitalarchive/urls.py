from django.urls import path
from .views import *

urlpatterns = [
    path(r'logauths',Authentication.as_view()),
    path(r'nextallowableactions',NextAllowableActions.as_view(),name='edge-create'),
    path(r'nextcrudactions',NextAllowableCrudAction.as_view(),name='edgecrud-create'),
    path(r'personnels',PersonnelApi.as_view(),name='personnel-create'),
    path(r'personnels/<int:id>', PersonnelDetail.as_view()), 

    path(r'ranks',RankApi.as_view(),name='rank-create'),
    path(r'events',EventApi.as_view(),name='event-create'),
    path(r'aircrafts',AircraftApi.as_view(),name='aircraft-create'),
    path(r'aircrafts/<int:id>',AircraftDetail.as_view()),
    path(r'secrecies',SecrecyApi.as_view(),name='secrecy-create'),
    path(r'studiophotographies',StudioPhotographyApi.as_view(),name='studiophotography-create'),

    path(r'assets',AssetApi.as_view(),name='asset-create'),
    path(r'galleries',GalleryApi.as_view(),name='asset-create'),
    path(r'assetalls',AssetAllApi.as_view(),name='asset-create'),
    path(r'assets/<int:id>', AssetDetail.as_view()), 

    path(r'locations',LocationApi.as_view(),name='location-create'),
    path(r'bofieldseditnoaccesses',GetNotEditableFields.as_view(),name='bofieldseditnoaccesses-create'),
    path(r'bofieldsviewnoaccesses',GetNotViewableFields.as_view(),name='bofieldsviewnoaccesses-create'),
    path(r'imagereferences',ImageReferenceApi.as_view(),name='imagereferences-create'),
    path(r'imageresults',imagerecognition,name='imagereferences-create'),
    path(r'gallery',galleryDirectory),
    path(r'insertgallery',insertgalleryDirectory),
    path(r'gallerydir',searchgallerydir),
    path(r'searches',TotalSearchApi.as_view()),
    path(r'advancesearches',AdvanceSearchApi.as_view()),
    path(r'bulkassets',bulkasset)

    
]
