from django.apps import AppConfig


class DigitalarchiveConfig(AppConfig):
    name = 'digitalarchive'
