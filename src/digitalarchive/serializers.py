from rest_framework import serializers
from .models import *
import jwt
from urllib.request import urlopen 
import base64
import requests
import os
import uuid

class ImageResultSerializer(serializers.ModelSerializer):
    class Meta:
        model=ImageResult
        fields=('id','x','y','height','width','imagedata')


class ImageReferenceSerializer(serializers.ModelSerializer):
    class Meta:
        model=ImageReference
        fields=('id','bdno','imagename','imagedirectory')


class AircraftSerializer(serializers.ModelSerializer):
    class Meta:
        model=Aircrafts
        fields=('id','aircraftid','name','description','status','imageurls')


class RankSerializer(serializers.ModelSerializer):
    class Meta:
        model=Rank
        fields=('id','name','code','description','rankno','orderid')

class AircraftSerializer(serializers.ModelSerializer):
    class Meta:
        model=Aircrafts
        fields=('id','aircraftid','name','description','status','imageurls')


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model=Location
        fields=('id','name','address','lat','lng')

class EventsSerializer(serializers.ModelSerializer):
    class Meta:
        model=Events
        fields=('id','name','date','description')

class SecrecySerializer(serializers.ModelSerializer):
    class Meta:
        model=Secrecy
        fields=('id','name')

class StudioPhotographySerializer(serializers.ModelSerializer):
    class Meta:
        model=StudioPhotography
        fields=('id','name')

class PersonnelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Personnel
        fields=('id','bdno','name','isretired','imageurls','rankhistory','status','currentrank','nameplate')


class SearchSerializer(serializers.ModelSerializer):
    class Meta:
        model=Asset
        fields=('id','imageurls','directory','title','assetid','taken')

class AdvanceSearchSerializer(serializers.ModelSerializer):
    class Meta:
        model=Asset
        fields=('id','imageurls','directory','title','assetid','taken')

class GallerySerializer(serializers.ModelSerializer):
    class Meta:
        model=Asset
        fields=('id','imageurls','directory','title','assetid','taken')

class AssetSerializer(serializers.ModelSerializer):
    imagedataarray = serializers.SerializerMethodField(source='get_imagedataarray')
    def get_imagedataarray(self,obj):
        data=Asset.objects.filter(assetid=obj).all()
        imagedata=""
        for d in data:
            response = requests.get(d.imageurls)
            if response.status_code == 200:
                image_data = response.content
                image_format = os.path.splitext(d.imageurls)[-1].replace('.', '').lower()
                encoded_string = base64.b64encode(image_data).decode('utf-8')
                if image_format in ['jpg', 'jpeg', 'png', 'gif']:
                    return 'data:image/%s;base64,%s' % (image_format, encoded_string)
                pass

    class Meta:
        model=Asset
        fields="__all__"

class AssetBulkSerializer(serializers.ModelSerializer):
    imagedataarray = serializers.SerializerMethodField(source='get_imagedataarray')
    def get_imagedataarray(self,obj):
        data=Asset.objects.filter(assetid=obj).all()
        imagedata=""
        for d in data:
            response = requests.get(d.imageurls)
            if response.status_code == 200:
                image_data = response.content
                image_format = os.path.splitext(d.imageurls)[-1].replace('.', '').lower()
                encoded_string = base64.b64encode(image_data).decode('utf-8')
                if image_format in ['jpg', 'jpeg', 'png', 'gif']:
                    return 'data:image/%s;base64,%s' % (image_format, encoded_string)
                pass

    class Meta:
        model=Asset
        fields="__all__"

class EdgeSerializer(serializers.ModelSerializer):
    class Meta:
        model=Edge
        fields=('id','objectname','nextaction','startstatus','endstatus','role')

class CrudEdgeSerializer(serializers.ModelSerializer):
    class Meta:
        model=CrudEdge
        fields=('id','objname','status','role','crudaction')


class UserSerializer(serializers.ModelSerializer):
    token = serializers.SerializerMethodField(source='get_token')
    role = serializers.SerializerMethodField(source='get_role')

    def get_token(self,obj):

        access_token_payload = {
            'user_id': obj.loginid,
            }
        access_token = str(uuid.uuid4().hex)
        return  access_token
    
    def get_role(self,obj):
        user=UserRole.objects.filter(loginid=obj.loginid)
        role=''
        for u in user:
            role=u.role
            pass
        return role
        
    class Meta:
        model=User
        fields=('id','name','loginid','password','token','role')

class BoFieldEditNoAccessSerializer(serializers.ModelSerializer):
    class Meta:
        model=BoFieldEditNoAccess
        fields=('id','user','role','businessobject','attribute')

class BoFieldViewNoAccessSerializer(serializers.ModelSerializer):
    class Meta:
        model=BoFieldViewNoAccess
        fields=('id','user','role','businessobject','attribute')